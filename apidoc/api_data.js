define({ "api": [
  {
    "type": "post",
    "url": "http://lsds.hesge.ch/icebound/pdop_processing/",
    "title": "PDOP",
    "group": "GNSS",
    "name": "PDOP",
    "description": "<p>Route that executes a PDOP calculation on a MNS for a given time span.</p>",
    "version": "0.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Session ID of the user (uuid)</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "date",
            "description": "<p>Date of the forecast, format must be dd/mm/yyyy</p>"
          },
          {
            "group": "Parameter",
            "type": "Hour",
            "optional": false,
            "field": "hour",
            "description": "<p>Starting hour of the forecast, format must be hh:mm</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "forecast_number",
            "description": "<p>Number of forecast to do</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "forecast_spacing",
            "description": "<p>Spacing between forecasts</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "constellations",
            "description": "<p>Constellations used in the measure, this value is comma separated</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude of the MNS's location</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude of the MNS's location</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "height",
            "description": "<p>Height of the antenna</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "resolution",
            "description": "<p>Resolution of the MNS</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "input_files",
            "description": "<p>zip archive that contains the MNS and the almanach file. The name of the MNS must be &quot;mns.tiff&quot; and the name of the almanach must be &quot;almanach.alc&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n   \"id\": \"1241434A\",\n   \"date\": \"06/02/1993\",\n   \"hour\": \"12:23\",\n   \"forecast_number\": 15,\n   \"forecast_spacing\": 20,\n   \"constellations\": \"Galileo,GPS\",\n   \"longitude\": 46.12,\n   \"latitude\": 12.46,\n   \"height\": 2,\n   \"resolution\": 0.5,\n   \"input_files\": {File},\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "URL",
            "description": "<p>Url of the zip archive that contains the results</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BadInputFormat",
            "description": "<p>The input data does not follow the API doc.</p>"
          }
        ]
      }
    },
    "filename": "./server.py",
    "groupTitle": "GNSS"
  },
  {
    "type": "post",
    "url": "http://lsds.hesge.ch/icebound/svf_processing/",
    "title": "Sky View Factor",
    "group": "Solar_Potential",
    "name": "Sky_View_Factor",
    "description": "<p>Route that executes a SVF calculation on a DUSM for a given sky model.</p>",
    "version": "0.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "with_front",
            "description": "<p>Do the calculation also on the front</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "resolution",
            "description": "<p>Resolution of the DUSM</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude of the DUSM's location</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "input_files",
            "description": "<p><b>zip</b> archive that contains the DUSM and the sky model. The name of the DUSM must be &quot;dusm.tif&quot; and the name of the sky model must be &quot;skymodel.mat&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n   \"with_front\": false,\n   \"resolution\": 0.5,\n   \"latitude\": 46.12,\n   \"input_files\": {File},\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "URL",
            "description": "<p>Url of the zip archive that contains the results</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BadInputFormat",
            "description": "<p>The input data does not follow the API doc.</p>"
          }
        ]
      }
    },
    "filename": "./server.py",
    "groupTitle": "Solar_Potential"
  },
  {
    "type": "post",
    "url": "http://lsds.hesge.ch/icebound/solar_processing/",
    "title": "Solar Radiation",
    "group": "Solar_Potential",
    "name": "Solar_Radiation",
    "description": "<p>Route that executes a Solar Radiation calculation on a DUSM for a given time span.</p>",
    "version": "0.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "with_front",
            "description": "<p>Do the calculation also on the front</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "yearly",
            "description": "<p>Do a yearly irradiation ?</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "month",
            "description": "<p>The month to process, not required whether it is a yearly irradiation</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "resolution",
            "description": "<p>Resolution of the DUSM</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude of the DUSM's location</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "input_files",
            "description": "<p>zip archive that contains the DUSM, the svf, the weather data, the orient file and the slope file.<br> The name of the DUSM must be &quot;dusm.tif&quot;<br> The name of the svf file must be &quot;skymodel.mat&quot;<br> The name of the weather file must be &quot;weather.mat&quot;<br> The name of the orient file must be &quot;orient.tif&quot;<br> The name of the slope file must be &quot;slope.tif&quot;<br> If front calculation, the frontSVF.txt, the frontOrient.tif and the frontDTM.tif should be provided.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n   \"with_front\": false,\n   \"yearly\" : false,\n   \"month\" : 1,\n   \"resolution\": 0.5,\n   \"latitude\": 46.12,\n   \"input_files\": {File},\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "URL",
            "description": "<p>Url of the zip archive that contains the results</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BadInputFormat",
            "description": "<p>The input data does not follow the API doc.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnconsistentInputFiles",
            "description": "<p>The input files are not consistent with the type of Solar Radiation calculation (Did you provide the front svf data ?)</p>"
          }
        ]
      }
    },
    "filename": "./server.py",
    "groupTitle": "Solar_Potential"
  }
] });
