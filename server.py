"""
@api {post} http://lsds.hesge.ch/icebound/pdop_processing/ PDOP
@apiGroup GNSS
@apiName PDOP

@apiDescription Route that executes a PDOP calculation on a MNS for a given time span.
@apiVersion 0.0.0

@apiParam {String} id Session ID of the user (uuid)
@apiParam {Date} date Date of the forecast, format must be dd/mm/yyyy
@apiParam {Hour} hour Starting hour of the forecast, format must be hh:mm
@apiParam {Number} forecast_number Number of forecast to do
@apiParam {Number} forecast_spacing Spacing between forecasts
@apiParam {String} constellations Constellations used in the measure, this value is comma separated
@apiParam {Number} longitude Longitude of the MNS's location
@apiParam {Number} latitude Latitude of the MNS's location
@apiParam {Number} height Height of the antenna
@apiParam {Number} resolution Resolution of the MNS
@apiParam {File} input_files zip archive that contains the MNS and the almanach file. The name of the MNS must be "mns.tiff" and the name of the almanach must be "almanach.alc"

@apiSuccess {String} URL Url of the zip archive that contains the results
@apiError BadInputFormat The input data does not follow the API doc.

@apiParamExample {json} Request-Example:
                 {
                    "id": "1241434A",
                    "date": "06/02/1993",
                    "hour": "12:23",
                    "forecast_number": 15,
                    "forecast_spacing": 20,
                    "constellations": "Galileo,GPS",
                    "longitude": 46.12,
                    "latitude": 12.46,
                    "height": 2,
                    "resolution": 0.5,
                    "input_files": {File},
                  }

"""
def pdop_entry_point():
    pass

"""
@api {post} http://lsds.hesge.ch/icebound/svf_processing/ Sky View Factor
@apiGroup Solar Potential
@apiName Sky View Factor
@apiDescription Route that executes a SVF calculation on a DUSM for a given sky model.
@apiVersion 0.0.0


@apiParam {Boolean} with_front Do the calculation also on the front
@apiParam {Number} resolution Resolution of the DUSM
@apiParam {Number} latitude Latitude of the DUSM's location
@apiParam {File} input_files <b>zip</b> archive that contains the DUSM and the sky model. The name of the DUSM must be "dusm.tif" and the name of the sky model must be "skymodel.mat"

@apiSuccess {String} URL Url of the zip archive that contains the results
@apiError BadInputFormat The input data does not follow the API doc.

@apiParamExample {json} Request-Example:
                 {
                    "with_front": false,
                    "resolution": 0.5,
                    "latitude": 46.12,
                    "input_files": {File},
                  }
"""
def svf_entry_point():
    pass

"""
@api {post} http://lsds.hesge.ch/icebound/solar_processing/ Solar Radiation
@apiGroup Solar Potential
@apiName Solar Radiation
@apiDescription Route that executes a Solar Radiation calculation on a DUSM for a given time span.
@apiVersion 0.0.0

@apiParam {Boolean} with_front Do the calculation also on the front
@apiParam {Boolean} yearly Do a yearly irradiation ?
@apiParam {Integer} month The month to process, not required whether it is a yearly irradiation
@apiParam {Number} resolution Resolution of the DUSM
@apiParam {Number} latitude Latitude of the DUSM's location
@apiParam {File} input_files zip archive that contains the DUSM, the svf, the weather data, the orient file and the slope file.\
                 The name of the DUSM must be "dusm.tif"\
                 The name of the svf file must be "skymodel.mat"\
                 The name of the weather file must be "weather.mat"\
                 The name of the orient file must be "orient.tif"\
                 The name of the slope file must be "slope.tif"\
                 If front calculation, the frontSVF.txt, the frontOrient.tif and the frontDTM.tif should be provided.

@apiSuccess {String} URL Url of the zip archive that contains the results

@apiError BadInputFormat The input data does not follow the API doc.
@apiError UnconsistentInputFiles The input files are not consistent with the type of Solar Radiation calculation (Did you provide the front svf data ?)

@apiParamExample {json} Request-Example:
                 {
                    "with_front": false,
                    "yearly" : false,
                    "month" : 1,
                    "resolution": 0.5,
                    "latitude": 46.12,
                    "input_files": {File},
                  }
"""
def solar_radiation_entry_point():
    pass
